import { createApp } from 'vue'

// import style file 
import './style.scss'

//import router file
import router from './router';

//import icon 
import svgCheckTask from "./components/icon/svgCheckTask.vue"
import svgTrash from "./components/icon/svgTrash.vue"
import svgEdit from "./components/icon/svgEdit.vue"
import svgCheck from "./components/icon/svgCheck.vue"
import svgCancel from "./components/icon/svgCancel.vue"
import svgFilter from "./components/icon/svgFilter.vue"

//import btn component 
import btn from "./components/btn.vue"

import App from './App.vue'

const app = createApp(App)
app.use(router)
app.component('svgCheckTask', svgCheckTask)
app.component("btn", btn)
app.component('svgTrash', svgTrash)
app.component('svgEdit', svgEdit)
app.component('svgCheck', svgCheck)
app.component('svgCancel', svgCancel)
app.component("svgFilter", svgFilter)
app.mount('#app')
