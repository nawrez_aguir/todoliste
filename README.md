Test Front-End VueJs3
Ce projet est une application Vuejs qui permettant de créer/modifier/supprimer une tache.

💻 Installation
Pour installer l'application, suivez ces étapes :
Clonez le projet de GitHub sur votre ordinateur local.
Accédez au répertoire du projet et exécutez npm install pour installer les dépendances requises.
Démarrez le serveur de développement en exécutant npm run dev

🔬 Documentation
Dans cette projet, vous pouvez voir la liste de toutes les taches existantes filtré par status. Vous pouvez créer une nouvelle tache en cliquant sur le bouton "Add" en haut à droite de la page. Pour modifier , supprimer ou modifier le status d'une tache existante, cliquez sur le bouton correspondant.

🎉Conclusion
C'est ça! Vous devriez maintenant avoir une application VueJS qui peut créer, mettre à jour, supprimer et afficher une tache . 😄

💬Contact
Si vous avez des questions ou des commentaires sur ce projet, n'hésitez pas à me contacter à nawrezaguir@gmail.com. J'aimerais recevoir de vos nouvelles !
